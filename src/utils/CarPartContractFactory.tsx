import { ChainConfigOverride, ChainNetConfig } from "@playcheck/standard-lib";
import Crypto from "crypto";
import { CarPart } from "../parts";
import { ChainType, ETHEREUM } from "../types";

class EthereumCarPart extends CarPart {
  constructor(networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super("Part", networkConfig, overrides);
  }
}

class SidechainCarPart extends CarPart  {
  constructor(networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super("BRPart", networkConfig, overrides);
  }
}

export default class CarPartContractFactory {
  static instances:{
    [chain:string]: CarPart
  } = {};
  constructor(){
    throw new Error("Use EthereumCarPartInstantiator.getCarPartContract()");
  }

  static getInstance(chain:ChainType, networkConfig:ChainNetConfig, overrides?:ChainConfigOverride) {
    const hashKey = Crypto.createHash('md5').update(`${JSON.stringify(networkConfig)}${!!overrides ? JSON.stringify(overrides) : ""}`).digest('hex');
    const mappingKey = `${chain}#${hashKey}`;

    if (!CarPartContractFactory.instances[mappingKey]) {
      if(chain === ETHEREUM){
        CarPartContractFactory.instances[mappingKey] = new EthereumCarPart(networkConfig, overrides);
      } else {
        CarPartContractFactory.instances[mappingKey] = new SidechainCarPart(networkConfig, overrides);
      }
      
    }
    return CarPartContractFactory.instances[mappingKey];
  }
}