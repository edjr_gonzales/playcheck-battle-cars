import { CarPartEdition, CarPartRarity } from ".";
import { TokenId } from "@playcheck/standard-lib";

export interface CarPartMetadataDetails {
  internalId: string;
  edition: CarPartEdition;
  brand: string;
  model: string;
  internalType: string;
  type: string;
  rarity: CarPartRarity,
  level?: number;
  weight: number,
  durability: number,
  speed: number,
  power: number,
  steering: number;
  isElite?:boolean;
  serialNumber?: string;
  birthTxnHash?: string;
};

export interface CarPartMetadata {
  id?: TokenId;
  name: string;
  owner?:string;
  description: string;
  image: string;
  external_url?:string;
  details: CarPartMetadataDetails;
  provisioningId?:string;
  crateDrawTx?:string;
  crateType?:string;
}

export interface LegendaryPartMarker {
  isLegendary: boolean;
}

export class CarPartMetadataDetail implements CarPartMetadataDetails {
  internalId = ""
  edition:CarPartEdition = "Prime"
  brand = ""
  model = ""
  internalType = ""
  type =  ""
  rarity:CarPartRarity = "Common"
  weight = 0
  durability = 0
  speed = 0
  power = 0
  steering = 0
  level = 1
  isElite:boolean = false
  serialNumber = ''
  birthTxnHash = ''
}