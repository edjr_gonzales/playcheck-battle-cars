import { getBunyanLogger } from "@playcheck/standard-lib";
import { ConnectParticipant } from "aws-sdk";
import { BRPartMetadata } from "../..";

const logger = getBunyanLogger('Utils:Opensea');

const OPENSEA_FORMAT_FIELDS = [
  "name", "description", "external_url", "image", "attributes",
  "image_data", "background_color", "animation_url", "youtube_url",
  "properties"
];

const intTypes = ["weight", "power", "durability", "steering", "speed"];
const strTypes = ["brand", "model", "rarity", "edition", "type"];

interface Trait {
  trait_type?: string;
  value: string|number;
}

function generateAttributes(instance:BRPartMetadata) {
  const attrs: Trait[] = [];
  const details:any = { ...instance.details };

  if (instance.details.isElite) {
    attrs.push({value: "Elite"});
  }

  // iterate on string types
  strTypes.forEach(t => {
    attrs.push({
      trait_type: t,
      value: details[t]
    });
  });


  // separate iteration for int types in case we need to add `max_value`
  intTypes.forEach(t => {
    attrs.push({
      trait_type: t,
      value: details[t]
    });
  });

  return attrs;
}

export function formatMetadata(instance:BRPartMetadata){
  logger.debug('formatting ', instance);

  const newInstance:any = { };
  const copy:any = instance;
  
  for(let prop in copy._allColumns()){
    logger.debug('instance prop', prop);
    if(OPENSEA_FORMAT_FIELDS.includes( prop.toLowerCase() )){
      newInstance[prop] = copy[prop];
      logger.debug('instance prop added', prop, copy[prop]);
    }
  }

  newInstance.attributes = generateAttributes(instance);

  return newInstance;
}

export { default as IOpenseaMetadata } from './ICrateMetadata';