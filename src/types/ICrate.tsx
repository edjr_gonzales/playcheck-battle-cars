import { IERC20 } from "@playcheck/standard-lib";
import { BasicCrateInfo, IOpenseaMetadata } from ".";

export default interface ICrate extends BasicCrateInfo, IOpenseaMetadata, IERC20 {
  //dropCount: () => number;
}