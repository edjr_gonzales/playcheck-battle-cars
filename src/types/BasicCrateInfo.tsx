import { EthereumAddress } from "@playcheck/standard-lib";
import { CrateEdition, CrateRarity } from ".";
import { CarPart } from "..";
import { BattleCarPart } from "../parts";

export default interface BasicCrateInfo {
  name:string;
  code:string;
  description:string;
  image:string;
  edition:CrateEdition;
  rarity:CrateRarity;
  openCrate: (partContract:CarPart, partsSourceAccount?:EthereumAddress) => Promise<Array<BattleCarPart> | false>;
}