import { DynamoDbTable, DynamoDbTableAttr, DynamoDbTableHash, DynamoDbTableRange, getBunyanLogger, IDynamoDbTable, TokenId } from "@playcheck/standard-lib";

const logger = getBunyanLogger('DB:RaceTokenData');

type RaceTokenRecordType = 'BuyEventPass' | 'TokenDelivery' | '';

type RaceTokenDataType = RaceTokenData | RaceTokenEventPassPurchase | RaceTokenDelivery;

export interface RaceTokenData extends IDynamoDbTable<any> {}
@DynamoDbTable('RaceTokenData')
export class RaceTokenData {

  @DynamoDbTableHash()
  raceTokenDataKey:string = '';

  @DynamoDbTableRange()
  raceTokenDataId: string = '';

  @DynamoDbTableAttr()
  ethWallet:string = '';

  @DynamoDbTableAttr()
  recordType:RaceTokenRecordType = '';
}

export class EventPassOrderData {
  transactionHash: string = '';
  orderRef: any = {};
  executedOn: number = 0;
  eventId: string = '';
  tokenSpent: number = 0;
}

export interface RaceTokenEventPassPurchase extends IDynamoDbTable<any> {}
@DynamoDbTable('RaceTokenData')
export class RaceTokenEventPassPurchase extends RaceTokenData {
  @DynamoDbTableAttr()
  recordType:RaceTokenRecordType = 'BuyEventPass';

  @DynamoDbTableAttr()
  orderId:string = '';

  @DynamoDbTableAttr()
  fee: number = 1;

  @DynamoDbTableAttr()
  value: TokenId  = '';

  @DynamoDbTableAttr()
  qty:number = 1;

  @DynamoDbTableAttr()
  payer:string  = '';

  @DynamoDbTableAttr()
  recipient:string  = '';

  @DynamoDbTableAttr()
  tx:string  = '';

  @DynamoDbTableAttr()
  txOn:number  = Date.now();

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: Object } })
  receipt?:any;

  @DynamoDbTableAttr()
  orderStatus: 'new' | 'low_balance' | 'no_transaction' | 'failed_transaction' | 'system_error' | 'complete'  = 'new';
}

export interface RaceTokenDelivery extends IDynamoDbTable<any> {}
@DynamoDbTable('RaceTokenData')
export class RaceTokenDelivery extends RaceTokenData {
  @DynamoDbTableAttr()
  recordType:RaceTokenRecordType = 'TokenDelivery';

  @DynamoDbTableAttr()
  orderId:string = '';

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: Object } })
  orderKey:any;

  @DynamoDbTableAttr()
  sentOn: number  = Date.now();

  @DynamoDbTableAttr()
  txVerifiedOn?:number;

  @DynamoDbTableAttr()
  tx:string  = '';

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: Object } })
  receipt?:any;
}

export const loadRaceTokenData = async (modelInstance:RaceTokenDataType, raceTokenDataKey:string, raceTokenDataId:string) => {
  const result = Object.assign(modelInstance, { raceTokenDataKey, raceTokenDataId });
  
  await result.load();
 
  return result;
}