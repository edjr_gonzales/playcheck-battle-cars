import { DynamoDbTable, DynamoDbTableAttr, DynamoDbTableHash, DynamoDbTableRange, getBunyanLogger, IDynamoDbTable, TokenId } from "@playcheck/standard-lib";

const logger = getBunyanLogger('DB:ScrapData');

type ScrapRecordType = 'Reward' | 'ClaimReward' | 'PartUpgrade' | 'BuyEventPass' | 'BuyRaceToken' | '';

type ScrapDataType = ScrapData | ScrapReward | ScrapRewardClaim | ScrapPartUpgrade | ScrapRaceTokenPurchase;

export interface ScrapData extends IDynamoDbTable<any> {}
export interface ScrapReward extends IDynamoDbTable<any> {}
export interface ScrapRewardClaim extends IDynamoDbTable<any> {}
export interface ScrapPartUpgrade extends IDynamoDbTable<any> {}
export interface ScrapRaceTokenPurchase extends IDynamoDbTable<any> {}

@DynamoDbTable('ScrapData')
export class ScrapData {

  @DynamoDbTableHash()
  scrapDataKey:string = '';

  @DynamoDbTableRange()
  scrapDataId: string = '';

  @DynamoDbTableAttr()
  ethWallet:string = '';

  @DynamoDbTableAttr()
  recordType:ScrapRecordType = '';
}

@DynamoDbTable('ScrapData')
export class ScrapReward extends ScrapData {
  @DynamoDbTableAttr()
  recordType: ScrapRecordType = 'Reward';

  @DynamoDbTableAttr()
  rewardId:string = '';

  @DynamoDbTableAttr()
  amount: number = 0;

  @DynamoDbTableAttr()
  grantedBy: string  = '';

  @DynamoDbTableAttr()
  grantedOn:number = Date.now();

  @DynamoDbTableAttr()
  claimStub:string  = '';

  @DynamoDbTableAttr()
  claimId?:string;
}

@DynamoDbTable('ScrapData')
export class ScrapRewardClaim extends ScrapData {
  @DynamoDbTableAttr()
  recordType:ScrapRecordType = 'ClaimReward';

  @DynamoDbTableAttr()
  claimId:string = '';

  @DynamoDbTableAttr({ castTo: { type: 'Array', memberType: String } })
  rewards: Array<string> = [];

  @DynamoDbTableAttr()
  claimData: string  = '';

  @DynamoDbTableAttr()
  claimedOn:number = Date.now();

  @DynamoDbTableAttr()
  claimStub:string  = '';

  @DynamoDbTableAttr()
  tx:string  = '';

  @DynamoDbTableAttr()
  txOn:number  = Date.now();

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: Object } })
  receipt?:any;

  @DynamoDbTableAttr()
  retryOf:string  = '';

  @DynamoDbTableAttr()
  status: 'success' | 'fail' | 'low_balance' | 'new'  = 'new';
}

export class PartUpgradeData {
  transactionHash: string = '';
  orderRef: any = {};
  executedOn: number = 0;
  fromLevel: number = 1;
  toLevel: number = 1;
  tokenSpent: number = 0;
}

@DynamoDbTable('ScrapData')
export class ScrapPartUpgrade extends ScrapData {
  @DynamoDbTableAttr()
  recordType: ScrapRecordType = 'PartUpgrade';

  @DynamoDbTableAttr()
  orderId:string = '';

  @DynamoDbTableAttr()
  fee: number = 1;

  @DynamoDbTableAttr()
  value: TokenId  = '';

  @DynamoDbTableAttr()
  qty:number = 1;

  @DynamoDbTableAttr()
  payer:string  = '';

  @DynamoDbTableAttr()
  recipient:string  = '';

  @DynamoDbTableAttr()
  tx:string  = '';

  @DynamoDbTableAttr()
  txOn:number  = Date.now();

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: Object } })
  receipt?:any;

  @DynamoDbTableAttr()
  orderStatus: 'new' | 'low_balance' | 'no_transaction' | 'failed_transaction' | 'system_error' | 'complete'  = 'new';
}

export class RaceTokenOrderData {
  transactionHash: string = '';
  orderRef: any = {};
  executedOn: number = 0;
  raceTokensOrdered: number = 1;
  scrapSpent: number = 0;
}

@DynamoDbTable('ScrapData')
export class ScrapRaceTokenPurchase extends ScrapPartUpgrade {
  @DynamoDbTableAttr()
  recordType:ScrapRecordType = 'BuyRaceToken';

  @DynamoDbTableAttr()
  value: number  = 1;
}

export const loadScrapData = async (modelInstance:ScrapDataType, scrapDataKey:string, scrapDataId:string) => {
  const result = Object.assign(modelInstance, { scrapDataKey, scrapDataId });
  
  await result.load();
 
  return result;
}