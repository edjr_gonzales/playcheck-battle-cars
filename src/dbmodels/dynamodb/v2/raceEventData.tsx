import { TransactionReceipt } from "@ethersproject/abstract-provider";
import { DynamoDbTable, DynamoDbTableAttr, DynamoDbTableHash, DynamoDbTableRange, getBunyanLogger, IDynamoDbTable, TokenId } from "@playcheck/standard-lib";

const logger = getBunyanLogger('DB:RaceEventData');

type RaceEventDataType = RaceEventData | RaceEventPass;

export interface RaceEventData extends IDynamoDbTable<any> {}
export interface RaceEventPass extends IDynamoDbTable<any> {}

@DynamoDbTable('RaceEventData')
export class RaceEventData {
  @DynamoDbTableHash()
  eventDataKey:string = '';

  @DynamoDbTableRange()
  eventDataId: string = '';

  @DynamoDbTableAttr()
  ethWallet:string = '';

  @DynamoDbTableAttr()
  recordType:string = '';
}

@DynamoDbTable('RaceEventData')
export class RaceEventPass extends RaceEventData {
  @DynamoDbTableAttr()
  recordType:string = 'EventPass';

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: Object } })
  raceTokenDataKey: any  = {};
}

export const loadRaceEventData = async (modelInstance:RaceEventDataType, eventDataKey:string, eventDataId:string) => {
  const result = Object.assign(modelInstance, { eventDataKey, eventDataId });
  
  await result.load();
 
  return result;
}