import { DynamoDbTable, DynamoDbTableAttr, DynamoDbTableHash, IDynamoDbTable, TokenId } from '@playcheck/standard-lib';
import { ChainType, FlagType, PartState, PART_STATE_READY, SUBCHAIN1 } from '../../..';
import { CarPartMetadata, CarPartMetadataDetail } from '../../../types/CarPartMetadata';

const MAX_LEVEL_MAPPING:{ [rarity: string]: number } = {
  'Common': Number(process.env.COMMON_PART_MAX_LEVEL || '2'),
  'Rare': Number(process.env.RARE_PART_MAX_LEVEL || '5'),
  'Epic': Number(process.env.EPIC_PART_MAX_LEVEL || '9')
}

export type BRMetadataType = BRPartMetadata | BRPartSystemMetadata;

export interface BRPartMetadata extends IDynamoDbTable<any> {}
export interface BRPartSystemMetadata extends IDynamoDbTable<any> {}

@DynamoDbTable('TokenMetadata')
export class BRPartMetadata implements CarPartMetadata {
  @DynamoDbTableHash()
  id?: number;

  @DynamoDbTableAttr()
  owner: string = "";

  @DynamoDbTableAttr()
  name: string = "";

  @DynamoDbTableAttr()
  description: string = "";

  @DynamoDbTableAttr()
  image: string = "";

  @DynamoDbTableAttr()
  external_url:string = "";

  @DynamoDbTableAttr({ castTo: { type: 'CustomClass', memberType: CarPartMetadataDetail } })
  details:CarPartMetadataDetail = new CarPartMetadataDetail;

  getMaxLevel(){
    return MAX_LEVEL_MAPPING[this.details.rarity] || 1;
  }

  getLevel(){
    return this.details.level || 1;
  }

  upgrade(multiplier: number = 1.04){
    if(this.getLevel() < this.getMaxLevel()){
      const ret = { 
        speed: [this.details.speed * 1],
        steering: [this.details.steering * 1],
        level: [this.getLevel()]
       };

      this.details.speed = Math.round(this.details.speed * multiplier);
      this.details.steering = Math.round(this.details.steering * multiplier);
      this.details.level = this.getLevel() + 1;

      ret.speed.push(this.details.speed);
      ret.steering.push(this.details.steering);
      ret.level.push(this.details.level);

      return ret;
    }
    
    return false;
  }
}

export class PartUpgradeDetail {
  upgrader: string = '';
  transactionHash: string = '';
  completedOn: number = 0;
  fromLevel: number = 1;
  toLevel: number = 1;
  resultingStat: string = "{}"; // JSON string
  tokenSpent: number = 0;
}

@DynamoDbTable('TokenMetadata')
export class BRPartSystemMetadata extends BRPartMetadata {
  @DynamoDbTableAttr()
  crateDrawTx?:string;

  @DynamoDbTableAttr()
  crateBox?:string;
  
  @DynamoDbTableAttr()
  chain:ChainType = SUBCHAIN1;

  @DynamoDbTableAttr()
  netChainId: string = "";

  @DynamoDbTableAttr()
  state: PartState = PART_STATE_READY;

  @DynamoDbTableAttr()
  sortId: string = "";

  @DynamoDbTableAttr()
  chainSortId:  string = "";

  @DynamoDbTableAttr()
  netChainSortId: string = "";

  @DynamoDbTableAttr()
  transferTo?: string;

  @DynamoDbTableAttr()
  transferTime?: number;

  @DynamoDbTableAttr()
  pendingTxnHash?: string;

  @DynamoDbTableAttr()
  transferComplete?: FlagType;

  @DynamoDbTableAttr()
  provisioningId?:number;

  @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: PartUpgradeDetail } })
  upgrades: {
    [orderId: string]: PartUpgradeDetail;
  } = {};
}

export const loadMetadataById = async (modelInstance:BRMetadataType, id:TokenId) => {
  const data = Object.assign(modelInstance, { id });

  await data.load()

  return data;
}
