import { DynamoDbTable, DynamoDbTableAttr, getBunyanLogger, IDynamoDbTable, TokenId, WalletAccount } from '@playcheck/standard-lib';
import { CarPartEdition, CarPartRarity } from '../../..';
import { CarPartMetadata, CarPartMetadataDetails } from '../../../types/CarPartMetadata';

const logger = getBunyanLogger('DB:Account');

type BRAccountType = BRAccount | AccountCrateHistory | AccountCrateInfo;

export interface BRAccount extends IDynamoDbTable<any> {}
export interface AccountCrateHistory extends IDynamoDbTable<any> {}
export interface AccountCrateInfo extends IDynamoDbTable<any> {}

export class CarPartMetadataDetailsAttr implements CarPartMetadataDetails {
  internalId: string = '';

  edition: CarPartEdition = 'Prime';

  brand: string = '';

  model: string = '';

  internalType: string = '';

  type: string = '';

  rarity: CarPartRarity = 'Common';

  weight: number = 0;

  durability: number = 0;

  speed: number = 0;

  power: number = 0;

  steering: number = 0;

  isElite?:boolean;

  serialNumber?: string;

  birthTxnHash?: string;
};

export class CarPartMetadataAttr implements CarPartMetadata  {
  id?: TokenId;

  name: string = '';

  owner?:string;

  description: string = '';

  image: string = '';

  external_url?:string;

  details: CarPartMetadataDetailsAttr = new CarPartMetadataDetailsAttr();

  provisioningId?:string;

  crateDrawTx?:string;

  crateType?:string;
}

export class ApprovalStatus {
  sideChain?: {
    [chain:number]: boolean;
  };

  mainChain?: {
    [chain:number]: boolean;
  };

  sideChainNetId?:string | number;

  sideChainId?:string | number;

  ethereumChainId?:string | number;

  ethereumNetId?:string | number;
}

export class CrateOpenTransaction {
  // network id where deposit took place
  networkId:string | number = 0;

  // type of crate for easy access, though we can find out using transaction hash (the long way)
  crateBox:string = '';

  // timestamp when the transaction occured, seconds since epoch
  postedAt:number = 0;
}

export class CrateOpenTransactionItem extends CrateOpenTransaction {
  // status
  status: 'completed' | 'failed' | 'invalid' | 'processing' = 'processing';

  // total parts given
  partsTotal:number = 0;
  // Part ID and when it was minted

  partIds:{
    [partId:number]:{
      transactionHash: string; // transaction hash of mint / transfer 
      transferredAt: number; // seconds from epoch
    };
  } = {};

  // completion time, seconds from epoch
  completedAt?:number;
}

export interface CrateOpenTransactionHistory {
  [txHash:string]: CrateOpenTransactionItem;
}

export class DrawnPartsCheckList {
  parts: CarPartMetadataAttr[] = [];

  done: boolean = false;

  crateBox: string = '';
}

@DynamoDbTable('Accounts')
export class BRAccount extends WalletAccount {
  constructor(){
    super();
  }
  
  @DynamoDbTableAttr({ castTo: { memberType: ApprovalStatus } })
  approvalStatus?: ApprovalStatus = {}

  @DynamoDbTableAttr()
  mainChainParts?:number;

  @DynamoDbTableAttr()
  sideChainParts?:number;

  @DynamoDbTableAttr()
  partsCountedOn?:number;
}

@DynamoDbTable('Accounts')
export class AccountCrateInfo extends BRAccount {
  @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: CrateOpenTransaction } })
  pendingCrateOpenTxn?: {
    [txHash:string]: CrateOpenTransaction;
  };
  
  /**
   * {
   *  "tx1": true,
   *  "tx2": false
   * }
   */

  /**
   * Old Apps use this
   */
  @DynamoDbTableAttr({ castTo: { type: 'Array', memberType: CarPartMetadataAttr } })
  mintedParts?: Array<CarPartMetadataAttr>/* CarPartMetadata[] */;

  @DynamoDbTableAttr()
  doneMinting?:boolean;

  /**
   * New Apps use this
   */
   @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: DrawnPartsCheckList } })
  crateOpenedParts?: {
    [txHash:string]:DrawnPartsCheckList
  };
}

/** instance with extensive content of attributes included */
@DynamoDbTable('Accounts')
export class AccountCrateHistory extends AccountCrateInfo {
  @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: Object } })
  completedCrateOpenTxn?: CrateOpenTransactionHistory;
}

export const loadAccountByWallet = async (modelInstance:BRAccountType, ethWallet:string) => {
  const result = Object.assign(modelInstance, { ethWallet });
  
  await result.load();
 
  return result;
}