import { getBunyanLogger, getDynamoDbDocClient, getDynamoTablePrefix, DynamoDbTable, DynamoDbTableHash, DynamoDbTableRange, DynamoDbTableAttr, IDynamoDbTable } from "@playcheck/standard-lib";

const logger = getBunyanLogger('DB:BattleRacers');

export interface BattleRacersVar extends IDynamoDbTable<any> {}

@DynamoDbTable('BattleRacers')
export class BattleRacersVar {

  @DynamoDbTableHash()
  id:string = "";

  @DynamoDbTableRange()
  primaryComposite: string = "";

  @DynamoDbTableAttr()
  value:string = "";
}

export const loadBattleRacersVar = async (instance:BattleRacersVar, id:string) => {
  const data:any = Object.assign(instance, { id, primaryComposite: id });
  await data.load();
  return data;  
}