import { Contract } from 'web3-eth-contract';

import {EthereumAddress, ChainNetConfig, ERC20, getContract, ChainConfigOverride, getEthereumConfig, mnemonicConverter, pKeyConverter} from '@playcheck/standard-lib';

export default class Crate extends ERC20 {
  private __crateName:string;
  protected _sourceAddress:EthereumAddress | null = null;
  
  constructor(contractName:string, networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    //const dappObjects = getContract(contractName, networkConfig, overrides);
    super({ ...getContract(contractName, networkConfig, overrides), contractName, networkConfig, addresses: [], overrides });

    this.__crateName = contractName;
  }

  protected async _initSourceAccount(partsHolderWalletIndex:number){
    const partsConnConfig = getEthereumConfig( process.env.SIDE_CHAIN_PARTS_ACCOUNT || 'sidechain-dev-parts' );
    const partsMnemonic = partsConnConfig.walletOptions.mnemonic?.phrase;
    const partsPrivateKeys = partsConnConfig.walletOptions.privateKeys;

    let addresses:string[] = [];

    if(partsMnemonic){
      addresses = await mnemonicConverter(partsMnemonic, partsConnConfig.walletOptions.addressIndex || 0, partsConnConfig.walletOptions.numberOfAddresses || 20);
    } else if(partsPrivateKeys.length > partsHolderWalletIndex){
      addresses = await pKeyConverter(partsPrivateKeys, partsConnConfig.walletOptions.addressIndex || 0, partsConnConfig.walletOptions.numberOfAddresses || 20);
    } else {
      throw new Error(`${this.contractName}: Unable to find mnemonic or private key for parts holder accounts at index ${partsHolderWalletIndex}`);
    }

    return this._sourceAddress = new EthereumAddress(addresses[ partsHolderWalletIndex ]);
  }

  set partsAccount(wallet:EthereumAddress | null){
    this._sourceAddress = wallet;
  }

  get partsAccount():EthereumAddress | null {
    return this._sourceAddress;
  }

  get contractName():string {
    return this.__crateName;
  }

  async purchase(amount:number, referrer:EthereumAddress, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.purchase(amount, referrer.$);
      },
      callbacks,
      `${await this.getPrimaryAccount()} purchases ${amount} of ${this.contractName}, referred by ${referrer.$}`
    );
  }

  async purchaseFor(buyer:EthereumAddress, amount:number, referrer:EthereumAddress, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.purchaseFor(buyer.$, amount, referrer.$);
      },
      callbacks,
      `${await this.getPrimaryAccount()} purchases ${amount} of ${this.contractName} for ${buyer.$}, referred by ${referrer.$}`
    );
  }

  async price():Promise<number>{
    if(!this._cache.cratePrice){
      this._cache.cratePrice = Number(await this._performContractCall(
        (contract:Contract) => {
          return contract.methods.price();
        },
        `getting ${this.contractName}'s price`
      ))
    }

    return this._cache.cratePrice;
  }
}