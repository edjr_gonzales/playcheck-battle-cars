import { ChainConfigOverride, ChainNetConfig, EthereumAddress, TokenId, mnemonicConverter } from "@playcheck/standard-lib";
import GameMetadata from "../data/json/data.json";
import CrateDrawBase from "./CrateDrawBase";
import { CarPart } from "../parts";

export default class PrimeCrateBase extends CrateDrawBase {
  constructor(contractName:string, ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, ethNetworkConfig, overrides);
    this.partsMetadataDB = GameMetadata.Parts;

    this._initSourceAccount(Number(process.env.PARTS_ACCOUNT_PRIME_LEGENDARY_IDX || 0)).then(wallet => {
      this._logger.debug(`${this.contractName} legendary parts source wallet -- ${wallet.$}`);
    });
  }

  /**
   * Delete this function no hardcoded values are no longer necessary
   * @param partContract 
   * @param sourceAccount 
   */
  protected async _drawLegendary(partContract:CarPart, sourceAccount:EthereumAddress):Promise<TokenId | false> {
    //number of legendaries according to old code
    //does this means we only have 61 legendaries 
    //starting from token 1 to 61?
    const magicNumber = 61;  
    const remaining = await partContract.balanceOf(sourceAccount);

    if(remaining > 0){
      return magicNumber - remaining;
      
    } else {
      return false;
      
    }
  }
}