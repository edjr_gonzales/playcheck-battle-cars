import { Crate } from ".";
import {ChainNetConfig, ChainConfigOverride} from "@playcheck/standard-lib";

export default class S1Crate extends Crate {
  constructor(contractName:string, networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super(contractName, networkConfig, overrides);
    
  }
}