import { ChainNetConfig, ChainConfigOverride, EthereumAddress, mnemonicConverter, getEthereumConfig, pKeyConverter } from "@playcheck/standard-lib";
import { CrateEdition, CrateRarity, ICrate } from "../types";
import { CarPart } from '..';
import { BattleCarPart } from '../parts';
import CratePoolDrawBase from './CratePoolDrawBase';

const symbol = "BRS1CKC";
const contractCode = "CKCrate";
const contractName = "Battle Racers S1 CK Crate";
const name = "CryptoKitties Crate";
const description = "CryptoKitties Crate from Battle Racers\n\n" +
"Contains 2 parts, each from the limited edition sets of RoboKitty MK-I and RoboKitty MK-II.\n\n" +
"More info at battleracers.io.";
const markdownDescription = "CryptoKitties Crate from Battle Racers\n" +
"Contains 2 parts, each from the limited edition sets of RoboKitty MK-I and RoboKitty MK-II.\n\n" +
"More info at [visible text](https://battleracers.io).";
const image = "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateCK.png";
const edition = "Cryptokitties";
const rarity = "";
const icon = "";
const externalUrl = "";
const animationUrl = "";
const youtubeUrl = "";
const attributes = [
  {
    "trait_type": "Edition", 
    "value": "CryptoKitties"
  }
];

export default class CKCrate extends CratePoolDrawBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CKCrate', ethNetworkConfig, overrides);
    this._initSourceAccount(Number(process.env.PARTS_ACCOUNT_CRYPTOKITTIES_IDX || 3)).then(wallet => {
      this._logger.debug(`${this.contractName} parts source wallet -- ${wallet.$}`);
    });
  }

  static contractSymbol():string {
    return symbol;
  }

  static contractName():string {
    return contractName;
  }

  static getName():string{
    return name;
  }

  get name():string{
    return name;
  }

  static getCode():string{
    return contractCode;
  }

  get code():string{
    return contractCode;
  }

  static getDescription():string{
    return description;
  }

  get description():string{
    return description;
  }

  static getImage():string {
    return image;
  }

  get image():string {
    return image;
  }

  static getEdition():CrateEdition {
    return edition;
  }

  get edition():CrateEdition {
    return edition;
  }

  static getRarity():CrateRarity {
    return rarity;
  }

  get rarity():CrateRarity {
    return rarity;
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  static getMarkdownDescription():string {
    return markdownDescription;
  }

  get metaMarkdownDescription():string {
    return markdownDescription;
  }

  get metaImage():string {
    return this.image;
  }

  static getIcon():string {
    return icon;
  }

  get metaIcon():string {
    return icon;
  }

  static getExternalUrl():string {
    return externalUrl;
  }
  
  get metaExternalUrl():string {
    return externalUrl;
  }

  static getAnimationUrl():string {
    return animationUrl;
  }

  get metaAnimationUrl():string {
    return animationUrl;
  }

  static getYoutubeUrl():string {
    return youtubeUrl;
  }

  get metaYoutubeUrl():string {
    return youtubeUrl;
  }

  static getAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  public async openCrate(partContract:CarPart, partsSourceAccount?:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    try {
      const witholdingAccount = !!partsSourceAccount ? partsSourceAccount : this.partsAccount;

      if(!witholdingAccount){
        this._logger.error(`${this.contractName} could not find the parts witholding account.`);
        return false;
      } else {
        return await this._drawParts(partContract, witholdingAccount, 2);
      }
    } catch (error) {
      this._logger.error(error);
      return false;
    }
    
  }
}