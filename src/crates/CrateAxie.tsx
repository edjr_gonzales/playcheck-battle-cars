import { ChainNetConfig, ChainConfigOverride, EthereumAddress, TokenId, mnemonicConverter, getEthereumConfig, pKeyConverter } from "@playcheck/standard-lib";
import { CrateEdition, CrateRarity, ICrate } from "../types";
import { BattleCarPart } from "../parts";
import { CarPart } from "..";
import CratePoolDrawBase from "./CratePoolDrawBase";

const symbol = "BRAXC";
const contractCode = "CrateAxie";
const contractName = "BR Axie Crate";
const name = "Axie Infinity Crate";
const description = "Axie Infinity Crate from Battle Racers\n\n" +
"Contains 4 parts, each from the limited edition sets of Chubby Carrot, Sleepless Bite, and Triple Nut Star.\n\n" +
"More info at battleracers.io.";
const markdownDescription = "Axie Infinity Crate from Battle Racers\n\n" +
"Contains 4 parts, each from the limited edition sets of Chubby Carrot, Sleepless Bite, and Triple Nut Star.\n\n" +
"More info at [visible text](https://battleracers.io)";
const image = "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateAxie.png";
const edition = "Axie";
const rarity = "";
const icon = "";
const externalUrl = "";
const animationUrl = "";
const youtubeUrl = "";
const attributes = [
  {
    "trait_type": "Edition", 
    "value": "Axie"
  }
];

export default class CrateAxie extends CratePoolDrawBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CrateAxie', ethNetworkConfig, overrides);
    this._initSourceAccount(Number(process.env.PARTS_ACCOUNT_AXIE_IDX || 1)).then(wallet => {
      this._logger.debug(`${this.contractName} parts source wallet -- ${wallet.$}`);
    });
  }

  static contractSymbol():string {
    return symbol;
  }

  static contractName():string {
    return contractName;
  }

  static getName():string{
    return name;
  }

  get name():string{
    return name;
  }

  static getCode():string{
    return contractCode;
  }

  get code():string{
    return contractCode;
  }

  static getDescription():string{
    return description;
  }

  get description():string{
    return description;
  }

  static getImage():string {
    return image;
  }

  get image():string {
    return image;
  }

  static getEdition():CrateEdition {
    return edition;
  }

  get edition():CrateEdition {
    return edition;
  }

  static getRarity():CrateRarity {
    return rarity;
  }

  get rarity():CrateRarity {
    return rarity;
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  static getMarkdownDescription():string {
    return markdownDescription;
  }

  get metaMarkdownDescription():string {
    return markdownDescription;
  }

  get metaImage():string {
    return this.image;
  }

  static getIcon():string {
    return icon;
  }

  get metaIcon():string {
    return icon;
  }

  static getExternalUrl():string {
    return externalUrl;
  }
  
  get metaExternalUrl():string {
    return externalUrl;
  }

  static getAnimationUrl():string {
    return animationUrl;
  }

  get metaAnimationUrl():string {
    return animationUrl;
  }

  static getYoutubeUrl():string {
    return youtubeUrl;
  }

  get metaYoutubeUrl():string {
    return youtubeUrl;
  }

  static getAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  /** I dont know the prupose of this, but for compatibility */
  async setErc721ContractAddress(erc721ContractAddr:EthereumAddress){
    this.contract.methods.setErc721ContractAddress(erc721ContractAddr).send();
  }

  public async openCrate(partContract:CarPart, partsSourceAccount?:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    try {
      const witholdingAccount = !!partsSourceAccount ? partsSourceAccount : this.partsAccount;

      if(!witholdingAccount){
        this._logger.error(`${this.contractName} could not find the parts witholding account.`);
        return false;
      } else {
        return await this._drawParts(partContract, witholdingAccount, 4);
      }
    } catch(error) {
      this._logger.error(error);
      return false;
    }
    
  }
}