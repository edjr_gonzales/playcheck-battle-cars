export { default as Crate } from "./Crate";
export { default as S1Crate } from "./S1Crate";

export { default as PrimeCrateBase } from "./PrimeCrateBase";
export { default as S1CrateBase } from "./S1CrateBase";

import { default as CKCrate } from "./CKCrate";
import { default as CrateAxie } from "./CrateAxie";
import { default as CrateWood } from "./CrateWood";
import { default as CrateBronze } from "./CrateBronze";
import { default as CrateSilver } from "./CrateSilver";
import { default as CrateGold } from "./CrateGold";
import { default as S1CrateWood } from "./S1CrateWood";
import { default as S1CrateBronze } from "./S1CrateBronze";
import { default as S1CrateSilver } from "./S1CrateSilver";
import { default as S1CrateGold } from "./S1CrateGold";

export { default as CKCrate } from "./CKCrate";
export { default as CrateAxie } from "./CrateAxie";
export { default as CrateWood } from "./CrateWood";
export { default as CrateBronze } from "./CrateBronze";
export { default as CrateSilver } from "./CrateSilver";
export { default as CrateGold } from "./CrateGold";
export { default as S1CrateWood } from "./S1CrateWood";
export { default as S1CrateBronze } from "./S1CrateBronze";
export { default as S1CrateSilver } from "./S1CrateSilver";
export { default as S1CrateGold } from "./S1CrateGold";

export const CRATE_LIST:{
  [className:string]: typeof CKCrate | typeof CrateAxie | typeof CrateWood | typeof CrateBronze | 
    typeof CrateSilver | typeof CrateGold | typeof S1CrateWood | typeof S1CrateBronze | 
    typeof S1CrateSilver | typeof S1CrateGold
} = {
  CKCrate, 
  CrateAxie, 
  CrateWood, CrateBronze, CrateSilver, CrateGold,
  S1CrateWood, S1CrateBronze, S1CrateSilver, S1CrateGold
};
