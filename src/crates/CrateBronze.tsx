import { ChainNetConfig, ChainConfigOverride, EthereumAddress, TokenId } from "@playcheck/standard-lib";
import { ICrate, CrateEdition, CrateRarity, DropRateConfig } from "../types";
import PrimeCrateBase from "./PrimeCrateBase";
import { CarPart } from "..";
import { BattleCarPart } from "../parts";

export const BRONZE_DROP_RATE:DropRateConfig = {
  legendary: 0.001935,
  elite: 0.03,
  parts: 2,
  rates: [0.67, 0.28, 0.05] // common, rare, epic
};

const symbol = "BRBZC";
const contractCode = "CrateBronze";
const contractName = "BR Bronze Crate";
const name = "Prime Bronze Crate";
const description = "Prime Crate from Battle Racers\n\n" +
"Contains 2 parts from the Prime collection. This crate has a high chance to drop Common parts.\n\n" +
"More info at battleracers.io.";
const markdownDescription = "Prime Crate from Battle Racers\n\n" +
"Contains 2 parts from the Prime collection. This crate has a high chance to drop Common parts.\n\n" +
"More info at [visible text](https://battleracers.io).";
const image = "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateBronzeShiny.png";
const edition = "Prime";
const rarity = "Bronze";
const icon = "";
const externalUrl = "";
const animationUrl = "";
const youtubeUrl = "";
const attributes = [
  {
    "trait_type": "Edition", 
    "value": "Prime"
  },
  {
    "trait_type": "Rarity", 
    "value": "Bronze"
  }
];

export default class CrateBronze extends PrimeCrateBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CrateBronze', ethNetworkConfig, overrides);
  }

  static contractSymbol():string {
    return symbol;
  }

  static contractName():string {
    return contractName;
  }

  static getName():string{
    return name;
  }

  get name():string{
    return name;
  }

  static getCode():string{
    return contractCode;
  }

  get code():string{
    return contractCode;
  }

  static getDescription():string{
    return description;
  }

  get description():string{
    return description;
  }

  static getImage():string {
    return image;
  }

  get image():string {
    return image;
  }

  static getEdition():CrateEdition {
    return edition;
  }

  get edition():CrateEdition {
    return edition;
  }

  static getRarity():CrateRarity {
    return rarity;
  }

  get rarity():CrateRarity {
    return rarity;
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  static getMarkdownDescription():string {
    return markdownDescription;
  }

  get metaMarkdownDescription():string {
    return markdownDescription;
  }

  get metaImage():string {
    return this.image;
  }

  static getIcon():string {
    return icon;
  }

  get metaIcon():string {
    return icon;
  }

  static getExternalUrl():string {
    return externalUrl;
  }
  
  get metaExternalUrl():string {
    return externalUrl;
  }

  static getAnimationUrl():string {
    return animationUrl;
  }

  get metaAnimationUrl():string {
    return animationUrl;
  }

  static getYoutubeUrl():string {
    return youtubeUrl;
  }

  get metaYoutubeUrl():string {
    return youtubeUrl;
  }

  static getAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  /**
   * Open crate and yield parts, either pre-existing or to be minted.
   * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
   * @param partsSourceAccount source wallet that owns legendary parts
   */
  public async openCrate(partContract:CarPart, partsSourceAccount?:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    if(!partsSourceAccount && this.partsAccount){
      return await this._openCrate(BRONZE_DROP_RATE, partContract, this.partsAccount);
    } else if(partsSourceAccount){
      return await this._openCrate(BRONZE_DROP_RATE, partContract, partsSourceAccount);
    } else {
      return false;
    }
  }
}