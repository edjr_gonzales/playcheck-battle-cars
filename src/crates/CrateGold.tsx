import { ChainNetConfig, ChainConfigOverride, EthereumAddress } from "@playcheck/standard-lib";
import { ICrate, CrateEdition, CrateRarity, DropRateConfig } from "../types";
import { CarPart } from "..";
import { BattleCarPart } from "../parts";
import PrimeCrateBase from "./PrimeCrateBase";

export const GOLD_DROP_RATE:DropRateConfig = {
  legendary: 0.024167,
  elite: 0.03,
  parts: 4,
  rates: [0.20, 0.50, 0.30] // common, rare, epic
};

const symbol = "BRGDC";
const contractCode = "CrateGold";
const contractName = "BR Gold Crate";
const name = "Prime Gold Crate";
const description = "Contains 4 parts from the Prime collection. This crate has the highest chance to drop Epic parts. More info at https://battleracers.io.";
const markdownDescription = "Contains 4 parts from the Prime collection. This crate has the highest chance to drop Epic parts. More info at [visible text](https://battleracers.io).";
const image = "https://s3-ap-southeast-1.amazonaws.com/assets.battleracers.io/images/crates/CrateGoldShiny.png";
const edition = "Prime";
const rarity = "Gold";
const icon = "";
const externalUrl = "";
const animationUrl = "";
const youtubeUrl = "";
const attributes = [
  {
    "trait_type": "Edition", 
    "value": "Prime"
  },
  {
    "trait_type": "Rarity", 
    "value": "Gold"
  }
];

export default class CrateGold extends PrimeCrateBase implements ICrate {
  constructor(ethNetworkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super('CrateGold', ethNetworkConfig, overrides);
  }

  static contractSymbol():string {
    return symbol;
  }

  static contractName():string {
    return contractName;
  }

  static getName():string{
    return name;
  }

  get name():string{
    return name;
  }

  static getCode():string{
    return contractCode;
  }

  get code():string{
    return contractCode;
  }

  static getDescription():string{
    return description;
  }

  get description():string{
    return description;
  }

  static getImage():string {
    return image;
  }

  get image():string {
    return image;
  }

  static getEdition():CrateEdition {
    return edition;
  }

  get edition():CrateEdition {
    return edition;
  }

  static getRarity():CrateRarity {
    return rarity;
  }

  get rarity():CrateRarity {
    return rarity;
  }

  get metaName():string {
    return this.name;
  }

  get metaDescription():string {
    return this.description;
  }

  static getMarkdownDescription():string {
    return markdownDescription;
  }

  get metaMarkdownDescription():string {
    return markdownDescription;
  }

  get metaImage():string {
    return this.image;
  }

  static getIcon():string {
    return icon;
  }

  get metaIcon():string {
    return icon;
  }

  static getExternalUrl():string {
    return externalUrl;
  }
  
  get metaExternalUrl():string {
    return externalUrl;
  }

  static getAnimationUrl():string {
    return animationUrl;
  }

  get metaAnimationUrl():string {
    return animationUrl;
  }

  static getYoutubeUrl():string {
    return youtubeUrl;
  }

  get metaYoutubeUrl():string {
    return youtubeUrl;
  }

  static getAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  get metaAttributes():Array<{
    [attr:string]: string | number | boolean;
  }> {
    return attributes;
  }

  /**
   * Open crate and yield parts, either pre-existing or to be minted.
   * @param partContract contract connecting to side chain -- e.g. Matic as of this moment
   * @param partsSourceAccount source wallet that owns legendary parts
   */
  public async openCrate(partContract:CarPart, partsSourceAccount?:EthereumAddress):Promise<Array<BattleCarPart> | false>{
    if(!partsSourceAccount && this.partsAccount){
      return await this._openCrate(GOLD_DROP_RATE, partContract, this.partsAccount);
    } else if(partsSourceAccount){
      return await this._openCrate(GOLD_DROP_RATE, partContract, partsSourceAccount);
    } else {
      return false;
    }
    
  }
}