import { Contract } from 'web3-eth-contract';
import { ChainConfigOverride, ChainNetConfig, ERC20, getContract, IMetaTransactable, MetaTransactable, Wallet } from "@playcheck/standard-lib";

export interface Scrap extends IMetaTransactable {}

@MetaTransactable
export class Scrap extends ERC20 {
  constructor(networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super({ ...getContract("ScrapV2", networkConfig, overrides), contractName: 'ScrapV2', networkConfig, addresses: [], overrides });
  }

  public async claimReward(rewardId:string, amount:number, signature:Buffer, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.claimReward(rewardId, amount, signature);
      },
      callbacks,
      `${await this.getPrimaryAccount()} performs claimReward with ID ${rewardId} amount ${amount}`
    );
  }

  public async withdrawOrderPayment(from:Wallet, orderId:string, amount:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.withdrawOrderPayment(from.$, orderId, amount);
      },
      callbacks,
      `${await this.getPrimaryAccount()} performs withdrawOrderPayment, from ${from.$} with orderId ${orderId}`
    );
  }

  public async withdrawEther(callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.withdrawEther();
      },
      callbacks,
      `${await this.getPrimaryAccount()} performs withdrawEther`
    );
  }
}