import { Contract } from 'web3-eth-contract';

import {EthereumAddress, ERC721, ChainNetConfig, TokenId, getContract, ChainConfigOverride, MetaTransactable, IMetaTransactable } from "@playcheck/standard-lib";

/**
 * Class contract spec for Battle Racers Part specifics
 */

export interface CarPart extends IMetaTransactable {}

@MetaTransactable
export class CarPart extends ERC721 {
  constructor(contractName:string, networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    const dappObjects = getContract(contractName, networkConfig, overrides);
    
    super({ ...dappObjects, contractName, networkConfig, addresses: [] });
  }

  async transferTokenFrom(from:EthereumAddress, to:EthereumAddress, tokenId:TokenId, callbacks?:{ [event:string]:Function }){
    return this._performContractSend((contract:Contract) => {
      return contract.methods.transferTokenFrom(from.$, to.$, tokenId);
    }, callbacks, `Transfer ${this.contractName} ${tokenId} from ${from.$} to ${to.$}`);
  }

  async transferFrom(from:EthereumAddress, to:EthereumAddress, tokenId:TokenId, callbacks?:{ [event:string]:Function }){
    return this.transferTokenFrom(from, to, tokenId, callbacks);
  }

  async mintPart(owner:EthereumAddress, tokenId:TokenId, tokenURI:string, callbacks?:{ [event:string]:Function }){
    return await this._performContractSend(
      (contract:Contract) => {
        return contract.methods.mintPart(owner.$, tokenId, tokenURI);
      },
      callbacks,
      `${await this.getPrimaryAccount()} mints ${this.contractName} ${tokenId} ${tokenURI} to account ${owner.$}`
    );
  }

  async mintPartSigned(owner:EthereumAddress, tokenId:TokenId, tokenURI:string, signature:Buffer, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.mintPartSigned(owner.$, tokenId, tokenURI, signature);
      },
      callbacks,
      `${await this.getPrimaryAccount()} performs signed mint ${this.contractName} ${tokenId} ${tokenURI} to account ${owner.$}`
    );
  }

  async enable(callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.enable();
      },
      callbacks,
      `${await this.getPrimaryAccount()} enables contracts ${this.contractName} ${this.address}`
    );
  }

  async disable(callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.disable();
      },
      callbacks,
      `${await this.getPrimaryAccount()} disabled contracts ${this.contractName} ${this.address}`
    );
  }
}
