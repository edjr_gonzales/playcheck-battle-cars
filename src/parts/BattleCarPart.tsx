import Web3 from "web3";
import Bunyan from "bunyan";
import { ADDRESS_0, EthereumAddress, TokenId } from "@playcheck/standard-lib";
import { AWSDynamoDB } from "../data";
import { CarPartMetadata, CarPartMetadataDetails, CarPartMetadataDetail } from "../types/CarPartMetadata";
import { ChainType, ETHEREUM, SUBCHAIN1, YES, NO, PART_STATE_TRANSIT, PART_STATE_READY  } from "../types";
import { CarPart } from ".";
import { BRPartMetadata, BRPartSystemMetadata } from "../dbmodels/dynamodb/legacy/metadata";
import { TransactionReceipt } from "@ethersproject/abstract-provider";
import { PromiseResult } from "aws-sdk/lib/request";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { AWSError } from "aws-sdk";


/**
 * Represents the car part that exists on both end of the chain - main chain and side chain
 */
export default class BattleCarPart implements CarPartMetadata {
  private __dynamodb;
  private __mainContract:CarPart;
  private __sideContract:CarPart;
  private __metadata:BRPartSystemMetadata | false = false;

  protected _logger:Bunyan;
  
  constructor(mainContract:CarPart, sideContract:CarPart, carPartId?:TokenId){
    this._logger = Bunyan.createLogger({
      name: `BattleCarPart`,
      level: process.env.NODE_ENV === 'development' ? Bunyan.DEBUG : Bunyan.INFO
    });

    this.__mainContract = mainContract;
    this.__sideContract = sideContract;
    this.__dynamodb = AWSDynamoDB.getInstance();

    if(carPartId){
      this.loadMetadata(carPartId);
    }
  }

  async balanceOf(wallet:EthereumAddress, chain:ChainType = ETHEREUM){
    return chain === ETHEREUM ? await this.__mainContract.balanceOf(wallet) : await this.__sideContract.balanceOf(wallet);
  }

  get id():TokenId {
    return this.__metadata ? (this.__metadata.id || 0) : 0;
  }

  get name():string {
    return this.__metadata ? this.__metadata.name : "";
  }

  get description(): string {
    return this.__metadata ? this.__metadata.description : "";
  }

  get image(): string {
    return this.__metadata ? this.__metadata.image : "";
  }

  get external_url(): string {
    return this.__metadata ? this.__metadata.external_url : "";
  }

  get details():CarPartMetadataDetails {
    return (this.__metadata && this.__metadata.details) || new CarPartMetadataDetail; 
  }

  get metadata():BRPartMetadata | false {
    return this.__metadata;
  }

  set metadata(metadata:BRPartMetadata | false) {
    this.__metadata = !!metadata ? Object.assign(new BRPartSystemMetadata, { ...metadata }) : metadata;
  }

  public async loadMetadata(tokenId:TokenId){
    const findPart:any = Object.assign(new BRPartSystemMetadata, { id: tokenId });

    findPart.load()
        .then(() => {
          this.__metadata = findPart;
        })
        .catch((err:any) => {
          this._logger.error(`Error finding part ID ${tokenId}`, err);
          throw err;
        });

      /* this._mapper.get(findPart)
        .then(partMetaData => {
          this.__metadata = partMetaData;
        })
        .catch(err => {
          this._logger.error(`Error finding part ID ${tokenId}`, err);
          throw err;
        }); */
  }

  public async isVerifiedOwner():Promise<boolean> {
    if(!!this.__metadata && !!this.__metadata.owner && !!this.__metadata.id){
      const dbRecordedOwner = new EthereumAddress(this.__metadata.owner);
      const chainRecognizedOwner = await this.__sideContract.ownerOf(this.__metadata.id);

      const result = dbRecordedOwner.equals(chainRecognizedOwner);
      if(!result){
        this._logger.debug(`Part token ${this.__metadata.id} owner discrepency. DB: ${dbRecordedOwner}, Network: ${chainRecognizedOwner}`);
      }

      return result;

    } else {
      return false;
    }
    
  }

  public async save(destinationWallet:EthereumAddress, crateBox:string = '', chain:ChainType = SUBCHAIN1){
    return new Promise<{ metadata:BRPartSystemMetadata, transactionHash:string } | false>(async (resolve, reject) => {
      this._logger.debug(`Saving part metadata`, this.__metadata, destinationWallet);
      this._logger.debug("addresses", this.__sideContract.addresses);

      if(this.__metadata){
        if(!this.id || this.id == 0){
          this._logger.debug(`\tFresh minting part...`);

          const METADATA_URL = process.env.METADATA_BASE_URL || "http://localhost/api/items/";
          
          //mint 
          const totalSupply = await this.__sideContract.totalSupply();

          let tokenId = Web3.utils.toBN(totalSupply).add(Web3.utils.toBN(1)).toString();

          const tokenURI = `${METADATA_URL}${tokenId}`;
          const netChainId = `CHAIN#${await this.__sideContract.web3.eth.net.getId()}`;

          // seek unused token ID
          let ownerLess = false;
          while(!ownerLess){
            try {
              await this.__sideContract.ownerOf(tokenId);

              // if owner is found, no error will be thrown, try another ID
              tokenId = Web3.utils.toBN(tokenId).add(Web3.utils.toBN(1)).toString();

            } catch(err) { // exception will be thrown by EthereumWallet class if reslting value is null
              ownerLess = true;
            }
          }

          // set provisioning ID
          this.__metadata.provisioningId = (new Date()).getTime();

          // assign designated birth chain chain
          this.__metadata.chain = chain;

          // assign sort ID
          this.__metadata.sortId = Web3.utils.toBN(tokenId).toString(16, 64);
          
          await this.__sideContract.mintPart(destinationWallet, tokenId, tokenURI, {
            'then': async (txReceipt:TransactionReceipt) => {
              if(txReceipt.status) {
                await this.__onPartTransactionCompleted(txReceipt.transactionHash, txReceipt.to, false);
                /* resolve({
                  metadata: this.__metadata as BRPartSystemMetadata,
                  transactionHash: txReceipt.transactionHash
                }); */
              } else {
                // delete metadata, reset the ID
                // await this._mapper.delete({ item: Object.assign(new BRPartSystemMetadata, { id: this.id }) });
                this.__metadata && await this.__metadata.destroy();
                this.__metadata && (this.__metadata.id = 0);
              }
            },
            'transactionHash': async (txHash:string) => {
              if(this.__metadata){ //dont worry, this is always true
                this.__metadata.crateBox = crateBox;
                this.__metadata.owner = ADDRESS_0;
                this.__metadata.transferTo = destinationWallet.$;
                this.__metadata.pendingTxnHash = txHash;
                this.__metadata.transferComplete = NO;
                this.__metadata.netChainId = netChainId;
                this.__metadata.netChainSortId = `${netChainId}_${this.__metadata.sortId}_${PART_STATE_TRANSIT}`;
                this.__metadata.chainSortId = `${this.__metadata.chain}_${this.__metadata.sortId}_${PART_STATE_TRANSIT}`;

                if(this.__metadata.details){
                  this.__metadata.details.birthTxnHash = txHash;
                }

                resolve({
                  metadata: this.__metadata as BRPartSystemMetadata,
                  transactionHash: txHash
                });

                // first time saving
                // await this._mapper.put({ item: this.__metadata });
                await this.__metadata.save();
              }
            }

          }).catch(async (reason:any) => {
            this._logger.error(`Part minting transaction failed!`, reason);

            if(await this.isVerifiedOwner()){
              // part is minted and is owned by destined owner
              this._logger.info(`An error has occurred but part ${this.__metadata && this.__metadata.id} is minted and is owned by destined owner ${destinationWallet.$}. Comitting this metadata to new owner...`);
              this.__onPartTransactionCompleted(false, destinationWallet.$, false);

              resolve({
                metadata: this.__metadata as BRPartSystemMetadata,
                transactionHash: (this.__metadata as BRPartSystemMetadata).details?.birthTxnHash || ""
              });
            } else {
              // remove metadata if it exists and provisioning ID matches
              if(this.__metadata && !!this.__metadata.id  && !!this.__metadata.provisioningId){
                /* await this._mapper.delete(Object.assign(
                  new BRPartSystemMetadata,
                  { id: this.__metadata.id, provisioningId: this.__metadata.provisioningId }
                )); */
                await this.__metadata.destroy();
              }

              reject(false);
            }

            //throw reason;
          });

        } else {
          this._logger.debug(`\tTransferring part from reserve account...`);

          const metadataOwner = new EthereumAddress(this.__metadata.owner);
          this._logger.debug(`\tMetadata Owner`, metadataOwner);

          const isOwnershipConsistent = await this.isVerifiedOwner();

          if(!isOwnershipConsistent){
            this._logger.info(`\tBattle Car Part ${this.__metadata.id} is being transferred to new owner ${destinationWallet.$} but the source account from DB does not match with network's actual owner`);
            throw new Error(`Battle Car Part ${this.__metadata.id} is being transferred to new owner ${destinationWallet.$} but the source account from DB does not match with network's actual owner`);
          }

          if(metadataOwner.equals(destinationWallet)){
            // already owned
            resolve({
              metadata: this.__metadata as BRPartSystemMetadata,
              transactionHash: this.__metadata.details.birthTxnHash || ""
            });
            
          } else {
            // not yet transferred
            // set sender as source account
            this.__sideContract.networkOverrides = { 
              ...this.__sideContract.networkOverrides, 
              options: { 
                ...this.__sideContract.networkOverrides?.options, 
                from: metadataOwner.$  //ensure that this account belongs to mnemonic used to initiate the contract
              } 
            };

            const operator = (this.__sideContract.networkOverrides.options?.from && new EthereumAddress(this.__sideContract.networkOverrides.options.from)) || await this.__sideContract.getPrimaryAccount();

            if(!metadataOwner.equals(operator)){
              const canTransfer = await this.__sideContract.isApprovedForAll(metadataOwner, operator);

              if(!canTransfer){
                this._logger.info(`\tPrimary account ${operator.$} is not approved for ${this.__metadata.owner} 's assets!`);
                reject(false);
                //throw new Error(`Primary account ${operator.$} is not approved for ${this.__metadata.owner} 's assets. Transfer will not continue.`);
              } 
            }

            await this.__sideContract.transferTokenFrom(metadataOwner, destinationWallet, this.id, {
              'transactionHash': async (txHash:string) => {
                if(this.__metadata){
                  this.__metadata.transferTo = destinationWallet.$;
                  this.__metadata.transferTime = Date.now();
                  this.__metadata.crateBox = crateBox;
                  this.__metadata.pendingTxnHash = txHash;
                  this.__metadata.transferComplete = NO;
                  this.__metadata.state = PART_STATE_TRANSIT;
                  this.__metadata.netChainSortId = `${this.__metadata.netChainId}_${this.__metadata.sortId}_${PART_STATE_TRANSIT}`;
                  this.__metadata.chainSortId = `${this.__metadata.chain}_${this.__metadata.sortId}_${PART_STATE_TRANSIT}`;

                  // update
                  // await this._mapper.update({ item: this.__metadata });
                  await this.__metadata.destroy();

                  resolve({
                    metadata: this.__metadata as BRPartSystemMetadata,
                    transactionHash: txHash
                  });
                }
              },
              'then': async (txReceipt:TransactionReceipt) => {
                if(txReceipt.status) {
                  await this.__onPartTransactionCompleted(txReceipt.transactionHash, txReceipt.to, true);
                  /* resolve({
                    metadata: this.__metadata as BRPartSystemMetadata,
                    transactionHash: (this.__metadata as BRPartSystemMetadata).details?.birthTxnHash || txReceipt.transactionHash
                  }); */
                } else {
                  //reject(false);
                }
              },
              
            }).catch(async (reason:any) => {
              this._logger.info(`Part transfer transaction failed!`, reason);
              reject(false);
              //throw reason;
            });
          }
        }
      }
    }); 
  }

  public async updateMetadata(): Promise<PromiseResult<DocumentClient.PutItemOutput | DocumentClient.UpdateItemOutput, AWSError> | false>{
    if(this.id && this.id !== 0){
      let existsInChain = true;

      try {
        this.__sideContract.ownerOf(this.id);
      } catch(err){
        existsInChain = false;
      }

      if(existsInChain){
        // return await this._mapper.put({ item: this.__metadata });
        return this.__metadata && await this.__metadata.save();
      } else {
        return false;
      }
    }

    return false;
  }

  private async __onPartTransactionCompleted(txHash:string | false, txTo:string, isTransfer:boolean){
    this._logger.info(`Part transaction completed, fully commiting ownership.`);
    
    if(this.__metadata){ //dont worry, this is always true on this scenario
      // refresh data from db
      await this.loadMetadata(this.id);

      const { owner: oldOwner, transferTo: newOwner } = this.__metadata;
      
      this._logger.debug(`Last Owner `, oldOwner, 'New Owner', newOwner, 'Transacted To', txTo);

      const EXTERNAL_URL = process.env.METADATA_EXT_URL || "http://localhost/items/";

      const part = this.__metadata;
      
      this.__metadata.state = PART_STATE_READY;
      this.__metadata.netChainSortId = `${this.__metadata.netChainId}_${PART_STATE_READY}`;
      this.__metadata.chainSortId = `${this.__metadata.chain}_${this.__metadata.sortId}_${PART_STATE_READY}`;
      
      this.__metadata.external_url = `${EXTERNAL_URL}${this.__metadata.id}`;
      this.__metadata.transferComplete = YES;
      this.__metadata.owner = txTo

      if(this.__metadata.details && !isTransfer){
        const partIdSerial = `${part.details?.brand}${part.details?.model}${part.details?.rarity}-${part.details?.internalType}`;
        const serial = Web3.utils.toBN(await this.__dynamodb.getSerialNum(partIdSerial));
        const newSerial = serial.add(Web3.utils.toBN(1)).toString();

        this.__metadata.details.serialNumber = newSerial;
        txHash && (this.__metadata.details.birthTxnHash = txHash);
      }

      delete(this.__metadata.provisioningId);

      // initially saved on onTransactionHash
      // use put to save or overwrite
      // await this._mapper.update({ item: this.__metadata });
      await this.__metadata.save();

      //update parts count cache for old owner account
      //if different from new owner
      if(oldOwner.toLowerCase() !== (newOwner || txTo).toLowerCase()){
        this._logger.debug(`Updating parts count for oldOwner`, oldOwner);
        await this.__dynamodb.updateAccountPartsCount(new EthereumAddress(oldOwner)); 
      }

      //update parts count cache for new owner account
      this._logger.debug(`Updating parts count for newOwner`, newOwner);
      await this.__dynamodb.updateAccountPartsCount(new EthereumAddress((newOwner || txTo)));

      this._logger.debug(`\tCompleted metadata commit:`, this.__metadata);
    }
  }
}