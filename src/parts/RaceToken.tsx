import { Contract } from 'web3-eth-contract';
import { ChainConfigOverride, ChainNetConfig, ERC20, getContract, IMetaTransactable, MetaTransactable, Wallet } from "@playcheck/standard-lib";

export class RaceToken extends ERC20 {
  constructor(networkConfig:ChainNetConfig, overrides?:ChainConfigOverride){
    super({ ...getContract("RaceToken", networkConfig, overrides), contractName: 'RaceToken', networkConfig, addresses: [], overrides });
  }

  public async withdrawOrderPayment(from:Wallet, orderId:string, amount:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.withdrawOrderPayment(from.$, orderId, amount);
      },
      callbacks,
      `${await this.getPrimaryAccount()} performs withdrawOrderPayment, from ${from.$} with orderId ${orderId}`
    );
  }

  public async withdrawEther(callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.withdrawEther();
      },
      callbacks,
      `${await this.getPrimaryAccount()} performs withdrawEther`
    );
  }
}